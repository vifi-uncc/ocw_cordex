This repository contains the Dockerfile for the OCW (Open Climate Workbench) and the CORDEX.

The built image is available at DockerHub as 'shambakey1/ocw_cordex' at 'https://hub.docker.com/r/shambakey1/ocw_cordex/'